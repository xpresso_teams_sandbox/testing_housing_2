MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_housing_2/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    strat_train_set = pickle.load(open(f"{PICKLE_PATH}/strat_train_set.pkl", "rb"))
    strat_test_set = pickle.load(open(f"{PICKLE_PATH}/strat_test_set.pkl", "rb"))
    strat_train_x = pickle.load(open(f"{PICKLE_PATH}/strat_train_x.pkl", "rb"))
    housing_labels = pickle.load(open(f"{PICKLE_PATH}/housing_labels.pkl", "rb"))
    housing_num = pickle.load(open(f"{PICKLE_PATH}/housing_num.pkl", "rb"))
    housing_cat = pickle.load(open(f"{PICKLE_PATH}/housing_cat.pkl", "rb"))
    housing_prepared = pickle.load(open(f"{PICKLE_PATH}/housing_prepared.pkl", "rb"))
    lin_reg = pickle.load(open(f"{PICKLE_PATH}/lin_reg.pkl", "rb"))
    tree_reg = pickle.load(open(f"{PICKLE_PATH}/tree_reg.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xpr_param_component_name = deci_tree_model	
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH","strat_train_set", "strat_test_set","strat_train_x","housing_labels","housing_num","housing_cat","housing_prepared","lin_reg","tree_reg"]



tree_reg = DecisionTreeRegressor(random_state=42)
tree_reg.fit(housing_prepared, housing_labels)
housing_predictions = tree_reg.predict(housing_prepared)
tree_mse = mean_squared_error(housing_labels, housing_predictions)
tree_rmse = np.sqrt(tree_mse)
tree_rmse


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/testing_housing_2/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(strat_train_set, open(f"{PICKLE_PATH}/strat_train_set.pkl", "wb"))
    pickle.dump(strat_test_set, open(f"{PICKLE_PATH}/strat_test_set.pkl", "wb"))
    pickle.dump(strat_train_x, open(f"{PICKLE_PATH}/strat_train_x.pkl", "wb"))
    pickle.dump(housing_labels, open(f"{PICKLE_PATH}/housing_labels.pkl", "wb"))
    pickle.dump(housing_num, open(f"{PICKLE_PATH}/housing_num.pkl", "wb"))
    pickle.dump(housing_cat, open(f"{PICKLE_PATH}/housing_cat.pkl", "wb"))
    pickle.dump(housing_prepared, open(f"{PICKLE_PATH}/housing_prepared.pkl", "wb"))
    pickle.dump(lin_reg, open(f"{PICKLE_PATH}/lin_reg.pkl", "wb"))
    pickle.dump(tree_reg, open(f"{PICKLE_PATH}/tree_reg.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
